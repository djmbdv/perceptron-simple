#include <SDL2/SDL.h>
#include <cstdio>
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 500;
double maxX,maxY, minX,minY;
const double umbral = 0.5;
const double aprendizaje = 0.00000001;
SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
void trans(double x, double y, int &xi, int &yi);
struct punto{
	double b;
	double x;
	double y;
};
struct caso{
	punto p;
	double se;
};
class perceptron{
	double pesos[3];	
public:
	vector <caso> conjunto_entrenamiento;
	perceptron();
	double evaluar(punto p);
	double entrenar(caso c);
	void drawLine(SDL_Renderer * r);
	double * get_pesos();
};
double * perceptron::get_pesos(){
	return pesos;
}
perceptron::perceptron(){
	pesos[0] = 0;
	pesos[1] = 0;
	pesos[2] = 0;
}
double perceptron::evaluar(punto p){
	return (pesos[0]*p.b + pesos[1]*p.x+pesos[2]*p.y > umbral);
}
void perceptron::drawLine(SDL_Renderer * r){
	double p1 = ((umbral - pesos[0] - pesos[1]*(minX - (maxX-minX)))/pesos[2]);
	double p2 = ((umbral - pesos[0] - pesos[1]*(maxX + (maxX - minX)))/pesos[2]);
	if(pesos[2] == 0)return;
	int x1,y1,x2,y2;
	trans ((minX - (maxX-minX)),p1,x1,y1);
	trans ((maxX + (maxX - minX)),p2,x2,y2);
	SDL_SetRenderDrawColor(r, 0, 0xFF, 0, 0xFF);
	SDL_RenderDrawLine(r,x1,y1,x2,y2);
}	
double perceptron::entrenar(caso c){
	double s = evaluar(c.p);
	pesos[0] += aprendizaje*(c.se - s)*c.p.b;
	pesos[1] += aprendizaje*(c.se - s)*c.p.x;
	pesos[2] += aprendizaje*(c.se - s)*c.p.y;
	return c.se - s;
}

int entrenamiento(void *data ){
	perceptron *p = (perceptron *)data;
	int errores;
	cout << "comienza a entrenar"<< endl;
	double error;
	do{
		errores = 0;
	///	cout << "pesos > w0 = " << p->get_pesos ()[0] << " w1 = " <<  p->get_pesos ()[1]  << " w2 = " <<  p->get_pesos ()[2]  << endl;
		for(int i = 0; i < p->conjunto_entrenamiento.size(); i++){
			if((error = p->entrenar(p->conjunto_entrenamiento[i])) != 0)errores++; 
	//		cout << "pesos > w0 = " << p->get_pesos ()[0] << " w1 = " <<  p->get_pesos ()[1]  << " w2 = " <<  p->get_pesos ()[2] << " error " << error << endl;
		}
	//	cout << "------------------------------------------" << endl;
	}while(errores);
	cout << "termina entrenar" << endl;
	return 0;
}
bool init(){
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )return false;
	if(!SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" )){
			printf( "Hint no habilitado" );
	}
	gWindow = SDL_CreateWindow( "RNA: Perceptrón Simple", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
	if( gWindow == NULL )return false;
	gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
	if( gRenderer == NULL )return false;
	SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
	return true;
}
typedef int32_t s32;
void DrawCircle(SDL_Renderer *Renderer, s32 _x, s32 _y, s32 radius){ 
	s32 x = radius - 1;
	s32 y = 0;
	s32 tx = 1;
	s32 ty = 1;
	s32 err = tx - (radius << 1); 
	while (x >= y){
		SDL_RenderDrawPoint(Renderer, _x + x, _y - y);
		SDL_RenderDrawPoint(Renderer, _x + x, _y + y);
		SDL_RenderDrawPoint(Renderer, _x - x, _y - y);
		SDL_RenderDrawPoint(Renderer, _x - x, _y + y);
		SDL_RenderDrawPoint(Renderer, _x + y, _y - x);
		SDL_RenderDrawPoint(Renderer, _x + y, _y + x);
		SDL_RenderDrawPoint(Renderer, _x - y, _y - x);
		SDL_RenderDrawPoint(Renderer, _x - y, _y + x);
		if (err <= 0){
			y++;
			err += ty;
			ty += 2;
		}else{
			x--;
			tx += 1;
			err += tx - (radius << 1);
		}
	}
}


void close(){
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	SDL_Quit();
}


void trans(double x, double y, int &xi, int &yi){
	xi = (int)( 20 +  ((x-minX)/(maxX - minX) ) * (SCREEN_WIDTH - 40));
	yi = (int)(SCREEN_HEIGHT - 20 - ((y - minY)/(maxY - minY) ) * (SCREEN_HEIGHT - 40));
}

int main( int argc, char* args[] ){
	bool quit = false;
	SDL_Event e;
	int n,x,y;
	SDL_Thread *thread;
	cin >> n;
	perceptron *p = new perceptron;
	double input[4];
	for(int i = 0; i < 4; i++)cin >> input[i];
	p->conjunto_entrenamiento.push_back({{input[0],input[1],input[2]},input[3]});
	minX = maxX = input[1];
	maxY = minY = input[2];
	while(--n){
		for(int i = 0; i < 4; i++)cin >> input[i];
		p->conjunto_entrenamiento.push_back({{input[0],input[1],input[2]},input[3]});
		if(input[1] > maxX) maxX = input[1];
		if(input[2] > maxY) maxY = input[2];
		if(input[1] < minX) minX = input[1];
		if(input[2] < minY) minY = input[2];
	}
	thread = SDL_CreateThread(entrenamiento, "Entrenamiento", (void *)p);
	if(!init()){
		cerr << "error al inicializar" << endl;
		return -1;
	}
	while( !quit ){
		while( SDL_PollEvent( &e ) != 0 ){
			if( e.type == SDL_QUIT ){
				quit = true;
			}
		}
		SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
		SDL_RenderClear( gRenderer );		
		SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0xFF, 0xFF );
		for(int i = 0; i < p->conjunto_entrenamiento.size(); i++){
			trans(p->conjunto_entrenamiento[i].p.x,p->conjunto_entrenamiento[i].p.y,x,y);
			if(p->conjunto_entrenamiento[i].se == 0)
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0xFF, 0xFF );
			else SDL_SetRenderDrawColor( gRenderer, 0xFF, 0x00, 0xFF, 0xFF );
			DrawCircle( gRenderer,x,y,3);
		}	
		p->drawLine (gRenderer);
		SDL_SetRenderDrawColor( gRenderer, 0, 0, 0, 0xFF );
		SDL_RenderPresent(gRenderer);
	}
	close();
	return 0;
}